//
//  Installer.swift
//  MobiCare
//
//  Created by Nandhini on 29/01/19.
//  Copyright © 2019 Nandhini. All rights reserved.
//

import UIKit

class Installer: NSObject {
    
    public func GetStory (storyName: String, identifier : String) -> UIViewController{
        return UIStoryboard(name: storyName, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    func navigationSetup () {
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor  = colors.main
        
        UINavigationItem().backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)

        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name:"Avenir Next Demi Bold", size:17.0)!]
        
         UINavigationController().navigationBar.setBackgroundImage(UIImage(named: ""), for: UIBarMetrics.default)
        UINavigationController().navigationBar.shadowImage = UIImage(named: "")
        
        UITextField.appearance().keyboardAppearance = .dark
        
    }
    
}
extension String{
    
    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isPhoneNumber: Bool {
        let phoneRegex = "^(()|(00))[0-9]{6,14}$"
        let PhoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegex)
        return PhoneTest.evaluate(with: self)
    }

}
extension UIViewController{
    
    open func showAlert (title:String?,message:String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:  "Ok", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
}
