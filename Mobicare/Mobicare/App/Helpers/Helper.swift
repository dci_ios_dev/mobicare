//
//  Helper.swift
//  MobiCare
//
//  Created by Nandhini on 29/01/19.
//  Copyright © 2019 Nandhini. All rights reserved.
//

import UIKit

class Helper: NSObject {
    
    //MARK: ViewBorder and ViewCorner
    func viewBorderCorner(view : UIView){
        
        view.layer.borderColor = colors.border.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        
        
    }
    
    //MARK: ButtonCorner and ButtonBorder
    
    func buttonCornerBorder(button : UIButton){
        
        button.layer.borderWidth = 1
        button.layer.borderColor = colors.border.cgColor
        button.layer.cornerRadius = 20
        button.clipsToBounds = true
        
    }
    
    
    
}
struct colors{
    
    static let border = UIColor.white
    static let main = UIColor(red: 75/255, green: 195/255, blue: 247/255, alpha: 1.0)
    
}
