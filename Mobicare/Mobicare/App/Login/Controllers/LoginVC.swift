//
//  LoginVC.swift
//  MobiCare
//
//  Created by Nandhini on 25/01/19.
//  Copyright © 2019 Nandhini. All rights reserved.
//

import UIKit

class LoginVC: UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var CreateProfileButton: UIButton!


    //MARK: ViewFlow
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Borders()
        self.TextFieldDelegates()
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    //MARK: StoryInitilize
    
    class func InitStory() -> LoginVC{
        
        let ViewC = Installer().GetStory(storyName: "LoginVC", identifier: "LoginVC")as! LoginVC
        return ViewC
        
    }
    
    
    //MARK: ViewButton_CornerBorder
    
    func Borders(){
        Helper().viewBorderCorner(view: EmailView)
        Helper().viewBorderCorner(view: PasswordView)
        Helper().buttonCornerBorder(button: LoginButton)
        Helper().buttonCornerBorder(button: CreateProfileButton)
    }
    
    
   
    //MARK: TextFieldDelegates
    
    func TextFieldDelegates(){
        
        self.EmailTextField.delegate = self
        self.PasswordTextField.delegate = self
        
        self.EmailTextField.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.PasswordTextField.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == EmailTextField{
            self.PasswordTextField.becomeFirstResponder()
        }
        else{
            self.PasswordTextField.resignFirstResponder()
        }
        return true
    }
    
    
    //MARK: Validation
    
    func Validation() -> Bool {
        
        let Email = self.EmailTextField.text!
        let Password = self.PasswordTextField.text!
        
        var Fields_Error = [String]()
        
        let Fields = [Email,Password]
        
        Fields_Error = ["Please Enter Email","Please Enter Password"]
        
        if ((Email.trimmingCharacters(in: .whitespaces).isEmpty) || !Email.isValidEmail()){
            if(Email.trimmingCharacters(in: .whitespaces).isEmpty){
                 self.showAlert(title: nil, message: "Email cannot be empty ")
            }else{
                 self.showAlert(title: nil, message: "Please Enter Valid Email ")
            }
           
            return false
        }
        else if(Password.trimmingCharacters(in: .whitespaces).isEmpty || Password.count < 6) || (Password.count > 12){
              self.showAlert(title: nil, message: "Password Must be More than 6 Character and Less than 12 characters")
            return false
        }
        else{
            for textField in 0...Fields.count - 1 {
                if Fields[textField].trimmingCharacters(in: .whitespaces).isEmpty{
                    self.showAlert(title: nil, message: Fields_Error[textField])
                    return false
                }
            }
        }
        print(Password.count)
//        if !Email.isValidEmail(){
//            self.showAlert(title: nil, message: "Please Enter a Valid Email")
//            return false
//        }
//
//        else if (Password.count < 6) || (Password.count > 12){
//            self.showAlert(title: nil, message: "Password Must be More than 6 Character and Less than 12 characters")
//            return false
//        }
        
        return true
    }
    
    
    //MARK: LoginAction
    @IBAction func LoginAct(_ sender: UIButton) {
        
        var params = [String:Any]()
        if self.Validation(){
           
        params = ["email":"\(EmailTextField.text!)","login_type":"1","password":"\(PasswordTextField.text!)","fcm":"123456"]
         
            MobicareApi.sharedInstance.loginApi(params: params, completion: { JSON in
                print("JSON RSponse \(JSON)")
                
            }) { Error in
                
            }
        }
    }
    
    //MARK: CreateProfileAction
    @IBAction func CreateProfileAct(_ sender: UIButton) {
      /*  let Email = EmailVerifyVC.InitStory()
        self.navigationController?.pushViewController(Email, animated: true)*/
    }
    
    //MARK: FacebookAction
    @IBAction func FaceBookAct(_ sender: UIButton) {
        
    }
    
    //MARK: GoogleAction
    @IBAction func GoogleAct(_ sender: UIButton) {
        
    }
    

}
