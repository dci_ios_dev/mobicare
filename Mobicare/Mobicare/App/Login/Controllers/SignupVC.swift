//
//  SignupVC.swift
//  MobiCare
//
//  Created by Nandhini on 29/01/19.
//  Copyright © 2019 Nandhini. All rights reserved.
//

import UIKit

class SignupVC: UIViewController{
    
    
    @IBOutlet weak var SignUpTable: UITableView!
    
    //MARK: ViewFlow
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableDelegate()
        // Do any additional setup after loading the view.
    }
    
    
    func TableDelegate(){
        self.SignUpTable.delegate = self
        self.SignUpTable.dataSource = self
    }
    
    class func InitStory() -> SignupVC{
        let ViewController = Installer().GetStory(storyName: "LoginVC", identifier: "SignupVC")as! SignupVC
        return ViewController
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SignupVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
