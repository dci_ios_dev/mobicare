//
//  EmailVerifyVC.swift
//  MobiCare
//
//  Created by Nandhini on 29/01/19.
//  Copyright © 2019 Nandhini. All rights reserved.
//

import UIKit

class EmailVerifyVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var VerifyButton: UIButton!
    @IBOutlet weak var EmailTextField: UITextField!
    
    
    //MARK: ViewFlow
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Register"
        
        self.CornerView()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: CornerOfView
    func CornerView(){
        
        Helper().viewBorderCorner(view: EmailView)
        Helper().buttonCornerBorder(button: VerifyButton)
        self.EmailTextField.delegate = self
         self.EmailTextField.attributedPlaceholder = NSAttributedString(string: "Enter your email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
   
    
    //MARK: StoryInitialize
    class func InitStory() -> EmailVerifyVC{
        let ViewController = Installer().GetStory(storyName: "LoginVC", identifier: "EmailVerifyVC")as! EmailVerifyVC
        return ViewController
    }
    
    //MARK: TextfieldDelegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    //MARK: Validation
    
    func Validation() -> Bool{
        
        let email = EmailTextField.text!
        
        if email.trimmingCharacters(in: .whitespaces).isEmpty{
            self.showAlert(title: nil, message: "Please Enter the Email")
            return false
        }
        
        if !email.isValidEmail(){
            self.showAlert(title: nil, message: "Please Enter Valid Email")
        }
        
        return true
        
    }
    
    //MARK: VerifyEmailAction
    @IBAction func VerifyButtonAction(_ sender: UIButton) {
        if Validation(){
            
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
   

}
