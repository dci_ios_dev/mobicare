//
//  HomeViewController.swift
//  Mobicare
//
//  Created by RamPrasath Perumal on 28/01/19.
//  Copyright © 2019 com.dci. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher


class homeCollectionsCell: UICollectionViewCell{
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryDescription: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var letsGoButton: UIButton!
    
//        {
//        didSet{
//            self.letsGoButton.layer.borderColor = Utilities.appColor.cgColor
//            self.letsGoButton.layer.cornerRadius = 2.0
//            self.letsGoButton.layer.borderWidth = 1.0
//        }
//    }
    
}


class HomeViewController: UIViewController {
//  Views from storyBoard
    @IBOutlet weak var homeCollections: UICollectionView!
    
    // data Assignments
    var gridCollectionlayout = GridCollectionLayout()
    var categoryData = [CategoryData]()
    
    
    
    // views created on runtime
    
    var floatingButton = UIButton()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.layoutFloatingButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeCollections.delegate = self
        homeCollections.dataSource = self
        self.homeCollections.collectionViewLayout = gridCollectionlayout
        let loginParams = [login.Email.getprams():"hentry@test.com",login.Password.getprams():"123456",login.Types.getprams():"1",login.FCM.getprams():"123456"]
        
        MobicareApi.sharedInstance.loginApi(params: loginParams, completion: { JSON in
            
            MobicareApi.sharedInstance.getPostData(url: Utilities.BASE_URL+"category", params: ["":""], completion: { Data in
                
                let decoder = JSONDecoder()
                do{
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let datas = try decoder.decode(Category_Base.self, from: Data)
                    guard let status = datas.status as? Int,status == 200  else{
                        return
                    }
                    self.categoryData = datas.data!
                    self.homeCollections.reloadData()
                    
                }catch{
                    print("Error")
                }
                
            }, error: { Error in
                
            })
            
        }) { Error in
            
        }
    }
    
    func layoutFloatingButton(){
        floatingButton = UIButton(type: .custom)
        floatingButton.translatesAutoresizingMaskIntoConstraints = false
        floatingButton.backgroundColor = .red
        floatingButton.setImage(UIImage(named: "plus"), for: .normal)
        DispatchQueue.main.async {
          
                self.view.addSubview(self.floatingButton)
                self.view.bringSubviewToFront(self.floatingButton)
                
                
                NSLayoutConstraint.activate([
                    self.view.trailingAnchor.constraint(equalTo: self.floatingButton.trailingAnchor, constant: 15),
                    self.view.bottomAnchor.constraint(equalTo: self.floatingButton.bottomAnchor, constant: 15),
                    self.floatingButton.widthAnchor.constraint(equalToConstant: 50),
                    self.floatingButton.heightAnchor.constraint(equalToConstant: 50),self.floatingButton.bottomAnchor.constraint(equalTo: self.tabBarController!.tabBar.topAnchor, constant: -20)])
           
        }
        
        
        self.floatingButton.layer.cornerRadius = 25
        self.floatingButton.layer.shadowColor = UIColor.black.cgColor
        self.floatingButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        self.floatingButton.layer.masksToBounds = false
        self.floatingButton.layer.shadowRadius = 2.0
        self.floatingButton.layer.shadowOpacity = 0.5
       
        
            

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.floatingButton.removeFromSuperview()
        
    }
    
    // handling touch for Menu button
    @IBAction func menuClicked(_ sender: Any) {
       // Utilities.showAlert()
        
    }
    // handling touch for notification clicked
    @IBAction func notificationClicked(_ sender: Any) {
    }
}
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCollectionsCell", for: indexPath) as! homeCollectionsCell
        cell.categoryDescription.text = self.categoryData[indexPath.row].name ?? ""
        cell.categoryImage.kf.setImage(with: URL(string:"\(self.categoryData[indexPath.row].image ?? "")"))
        cell.cardView.layer.cornerRadius = 10.0
        cell.cardView.layer.shadowColor = UIColor.black.cgColor
        cell.cardView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.cardView.layer.shadowRadius = 2.0
        cell.cardView.layer.shadowOpacity = 1.0
        cell.cardView.clipsToBounds = false
        cell.isSelected = false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = collectionView.cellForItem(at: indexPath) as! homeCollectionsCell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCollectionsCell", for: indexPath) as! homeCollectionsCell
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
        viewController.selectedCategoryId = self.categoryData[indexPath.row].id!
        self.navigationController?.pushViewController(viewController, animated: true)
        
      
//        if(Utilities.cellHeight.height == item.frame.height){
//            UIView.animate(withDuration: 1.0, animations: {
//                // self.view.bringSubviewToFront(collectionView)
//                collectionView.bringSubviewToFront(item)
//                item.frame.origin = item.frame.origin
//                item.frame.size.width = item.frame.width
//                item.frame.size.height = 2 * (item.frame.height)
//            })
//        }else{
//            UIView.animate(withDuration: 1.0, animations: {
//                // self.view.bringSubviewToFront(collectionView)
//
//                collectionView.sendSubviewToBack(item)
//                item.frame.origin = item.frame.origin
//                item.frame.size.width = item.frame.width
//                item.frame.size.height = Utilities.cellHeight.height
//
//            })
//        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let item = collectionView.cellForItem(at: indexPath) as! homeCollectionsCell
        
//        UIView.animate(withDuration: 1.0, animations: {
//            // self.view.bringSubviewToFront(collectionView)
//
//            collectionView.sendSubviewToBack(item)
//            item.frame.origin = item.frame.origin
//            item.frame.size.width = item.frame.width
//            item.frame.size.height = item.frame.height / 2
//
//    })
        
    }
    
    
    
}
