//
//  SubCategoryViewController.swift
//  Mobicare
//
//  Created by Gowsika on 29/01/19.
//  Copyright © 2019 com.dci. All rights reserved.
//

import UIKit
// JSON MODEL CLASS START

class Empty: Codable {
    let status: Int
    let data: DataClass
    
    init(status: Int, data: DataClass) {
        self.status = status
        self.data = data
    }
}

class DataClass: Codable {
    let category: [Category]
    let subcategory: [Subcategory]
    
    init(category: [Category], subcategory: [Subcategory]) {
        self.category = category
        self.subcategory = subcategory
    }
}

class Category: Codable {
    let categoryID: Int
    let categoryName: String
    let categoryImage: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case categoryName = "category_name"
        case categoryImage = "category_image"
        case description
    }
    
    init(categoryID: Int, categoryName: String, categoryImage: String, description: String) {
        self.categoryID = categoryID
        self.categoryName = categoryName
        self.categoryImage = categoryImage
        self.description = description
    }
}

class Subcategory: Codable {
    let subcategoryID: Int
    let subcategoryName: String
    let subcategoryImage: String
    
    enum CodingKeys: String, CodingKey {
        case subcategoryID = "subcategory_id"
        case subcategoryName = "subcategory_name"
        case subcategoryImage = "subcategory_image"
    }
    
    init(subcategoryID: Int, subcategoryName: String, subcategoryImage: String) {
        self.subcategoryID = subcategoryID
        self.subcategoryName = subcategoryName
        self.subcategoryImage = subcategoryImage
    }
}




// JSON MODEL CLASS END
class subCategoryCell:UICollectionViewCell{
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    
}

class MyHeaderFooterClass: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        
       
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}




class SubCategoryViewController: UIViewController {

    
    
    @IBOutlet weak var categoryCollections: UICollectionView!
    @IBOutlet weak var headerView: Header!
    
    var categoryData  = [Subcategory]()
    
    var gridCollectionLayoutAll = GridCollectionLayoutAll()
      let collectionViewHeaderFooterReuseIdentifier = "MyHeaderFooterClass"
    var selectedCategoryId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryCollections.delegate = self
        categoryCollections.dataSource = self
      
        gridCollectionLayoutAll.itemCount = 4
        gridCollectionLayoutAll.itemHeight = 100
      
        self.categoryCollections.collectionViewLayout = gridCollectionLayoutAll
        
        self.getDatFromApi()
        
       // self.categoryCollections.register(MyHeaderFooterClass.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionViewHeaderFooterReuseIdentifier)
        
        
    }
    
    func getDatFromApi(){
        
        MobicareApi.sharedInstance.getPostData(url: Utilities.BASE_URL+"subcategory", params: ["id":"\(self.selectedCategoryId)"], completion: { Data in
            
            let decoder = JSONDecoder()
            do{
                //decoder.keyDecodingStrategy = .convertFromSnakeCase
                let datas = try decoder.decode(Empty.self, from: Data)
                
                let empty = try? decoder.decode(Empty.self, from: Data)

                self.categoryData = (empty?.data.subcategory)!
                
               
                
                for cat in self.categoryData{
                    print("\(cat)")
                }
                
                guard let status = datas.status as? Int,status == 200  else{
                    return
                }
               self.categoryCollections.reloadData()
                
            }catch{
                print("Error")
            }
            
        }, error: { Error in
            
        })
        
        
    }
    

   

}
extension SubCategoryViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCategoryCell", for: indexPath) as! subCategoryCell
       
        cell.categoryImage.roundImage()
//        cell.categoryLabel.text = self.categoryData[indexPath.row].subcategoryName
//
//        cell.categoryImage.layer.masksToBounds = false
//        cell.categoryImage.layer.cornerRadius = cell.categoryImage.frame.width / 2
//        cell.categoryImage.clipsToBounds = true
//        cell.categoryImage.kf.setImage(with: URL(string:"\(self.categoryData[indexPath.row].subcategoryImage)"))
//        cell.categoryImage.backgroundColor = Utilities.appColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
       
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MyHeaderFooterClass", for: indexPath) as? MyHeaderFooterClass{
            //sectionHeader.sectionHeaderlabel.text = "Section \(indexPath.section)"
            return sectionHeader
        }
        return UICollectionReusableView()
        
    }
}
