//
//  Utilities.swift
//  Mobicare
//
//  Created by Gowsika on 28/01/19.
//  Copyright © 2019 com.dci. All rights reserved.
//

import Foundation
import UIKit

enum login:String{

    case Email = "email"
    case Types = "login_type"
    case Password = "password"
    case FCM = "fcm"
    
    func getprams() -> String{ return self.rawValue}
}

class Utilities:NSObject{
    static let BASE_URL = "http://mobicare.dci.in/api/"
    static var HeaderData = ""
    static var cellHeight = CGSize()
   static let appColor =  UIColor(red: 21.0/255.0, green: 197.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    static let instance = Utilities()
    
    func saveUserData(data:Any,name:String){
        UserDefaults.standard.set(data, forKey: name)
    }
    func getUserData(name:String) -> Any{
        
       return UserDefaults.standard.object(forKey: name)
    }
    
    
    
    func hideLoading(){
        
    }
    
    func showLoading(){
        
    }
   
    
}




extension UIView{
@IBDesignable
class CardView: UIView {
        @IBInspectable var cornerRadius: CGFloat = 20
        @IBInspectable var shadowOffsetWidth: Int = 0
        @IBInspectable var shadowOffsetHeight: Int = 3
        @IBInspectable var shadowColor: UIColor? = UIColor.black
        @IBInspectable var shadowOpacity: Float = 0.5
        override func layoutSubviews() {
            layer.cornerRadius = cornerRadius
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
            layer.masksToBounds = false
            layer.shadowColor = shadowColor?.cgColor
            layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
            layer.shadowOpacity = shadowOpacity
            layer.shadowPath = shadowPath.cgPath
        }
        
    }
}
extension UIImageView{
    func roundImage(){
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
}
