import UIKit

class GridCollectionLayoutAll: UICollectionViewFlowLayout {
    
    // here you can define the height of each cell
    var itemHeight: CGFloat = 120
    
    var itemCount : CGFloat = 3
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout()
    {
        //itemHeight = ((collectionView?.frame.size.width)! / 3) - 1
        minimumInteritemSpacing = 2
        minimumLineSpacing = 10
        scrollDirection = .vertical
        
    }
    
    func itemWidth() -> CGFloat
    {
        return ((collectionView?.frame.size.width)! / itemCount) - 4
    }
    
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemWidth())
        }
        get {
            Utilities.cellHeight = CGSize(width: itemWidth(), height: itemWidth())
            
            return CGSize(width: itemWidth(), height: itemWidth())
        }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
