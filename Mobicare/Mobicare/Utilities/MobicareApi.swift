//
//  MobicareApi.swift
//  Mobicare
//
//  Created by Gowsika on 28/01/19.
//  Copyright © 2019 com.dci. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



class MobicareApi:NSObject{
    
    static let sharedInstance = MobicareApi()
    func loginApi(params:[String:Any],completion:@escaping(JSON) -> Void,error:@escaping(Error) -> Void){
     
        Alamofire.request(Utilities.BASE_URL+"login", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result{
            case .success:
                Utilities.HeaderData = response.response?.allHeaderFields["jwt_token"] as? String ?? ""
                completion(JSON(response.result.value))
                break
            case .failure:
                error(response.result.error!)
                break
            }
        }
    }
    
    func getPostData(url:String,params:[String:Any],completion:@escaping(Data)->Void,error:@escaping(Error)->Void){
        let headers = ["Authorization": "Bearer \(Utilities.HeaderData)"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result{
            case .success:
               completion(response.data!)
                break
            case .failure:
                error(response.result.error!)
                break
            }
        }
    }
}
