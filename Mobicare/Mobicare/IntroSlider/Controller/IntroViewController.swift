//
//  IntroViewController.swift
//  Mobicare
//
//  Created by Gowsika on 25/01/19.
//  Copyright © 2019 com.dci. All rights reserved.
//

import UIKit

class IntroViewController: UIPageViewController {

    var arrViewControllers = [Slider]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

       // let viewController = Slider(nibName: "View", bundle: nil)
       
       
        self.dataSource = self
        
     //   self.navigationController?.pushViewController(myViewController, animated: true)
        
       
    }
}
extension IntroViewController:UIPageViewControllerDataSource,UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = arrViewControllers.index(of: viewController as! Slider) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return arrViewControllers.last }
        
        guard arrViewControllers.count > previousIndex else { return nil }
        
        return arrViewControllers[previousIndex]

        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = arrViewControllers.index(of: viewController as! Slider) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < arrViewControllers.count else { return arrViewControllers.first }
        
        guard arrViewControllers.count > nextIndex else { return nil }
        
        return arrViewControllers[nextIndex]
    }
    
    
  
    
    
}

