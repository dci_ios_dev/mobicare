//
//  ViewController.swift
//  Mobicare
//
//  Created by Gowsika on 25/01/19.
//  Copyright © 2019 com.dci. All rights reserved.
//

import UIKit

class ViewController: IntroViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        let myViewController1 : Slider = Slider(nibName: "View", bundle: nil)
        myViewController1.view.backgroundColor = UIColor.blue
        let myViewController2 : Slider = Slider(nibName: "View", bundle: nil)
        myViewController2.view.backgroundColor = UIColor.blue
        let myViewController3 : Slider = Slider(nibName: "View", bundle: nil)
        myViewController3.view.backgroundColor = UIColor.blue
        
        
        arrViewControllers.append(myViewController1)
        arrViewControllers.append(myViewController2)
        arrViewControllers.append(myViewController3)
        
        if let firstVC = arrViewControllers.first
        {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }


}
extension ViewController {
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return arrViewControllers.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }

    
}

